﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
namespace Realsense
{


    public class HandsController : MonoBehaviour
    {
        public bool trackHands = true;
        public HandsEventController.EventHandDetected m_HandDetectedEvent;
        private const int MAX_FINGERS = 10;
        private const int MAX_CONTOUR_SIZE = 65000;

        public int maxTrackedHands = 1;
        public int minDistanceMM = 350;
        public int maxDistanceMM = 1000;
        public int minHandArea = 3000;
        public int maxHandArea = 15000;
        public int minConvexityDefectDepth = 20;
        public int maxConvexityDefectDepth = 80;

        public Realsense.Hand[] mHands;
        private int validHandsCount = 0;
        IEnumerator SetParameters()
        {
            yield return new WaitForSeconds(3.0f);
            HandParams p;

            p.handCount = maxTrackedHands;
            p.minAreaSize = minHandArea;
            p.maxAreaSize = maxHandArea;
            p.minDefectDepth=minConvexityDefectDepth;
            p.maxDefectDepth=maxConvexityDefectDepth;
            p.minDepthTreshold=minDistanceMM;
            p.maxDepthTreshold=maxDistanceMM;            
            Realsense.HandDetector.SetParam(p);
        }
        // Use this for initialization
        void Start()
        {
            mHands = Hand.createArray(maxTrackedHands);
            m_HandDetectedEvent = new HandsEventController.EventHandDetected();
            StartCoroutine(SetParameters());

        }

        // Update is called once per frame
        void Update()
        {
            if (trackHands)
                getHandsData();

        }


        int getHandsData()
        {

            validHandsCount = Realsense.HandDetector.FindHands();
            for (int i = 0; i < validHandsCount && i < mHands.Length; i++)
            {
                if (Realsense.HandDetector.GetHand(i, ref mHands[i]))
                    m_HandDetectedEvent.Invoke(i, mHands[i]);
                else
                    Debug.LogError("Couldnt get hand...");

            }
            return validHandsCount;
        }

    }


}

