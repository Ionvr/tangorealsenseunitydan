﻿using UnityEngine;
using System.Collections;
namespace Realsense
{
    public class CameraManager : MonoBehaviour
    {        
        private AndroidJavaObject mRealsense;
        // Use this for initialization       
        void Start()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject appContext = jc.GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getApplicationContext");
                mRealsense = new AndroidJavaObject("com.intel.realsense.imageprocessing.RealsenseImageProcessor");
                mRealsense.Call("initCamera", appContext);
                mRealsense.Call("start");
            }

        }

        void Stop()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                mRealsense.Call("stop");                
            }
        }


    }
}