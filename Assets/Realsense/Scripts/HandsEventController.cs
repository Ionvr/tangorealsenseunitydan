﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using System.Collections.Generic;

namespace Realsense
{
    public class HandsEventController : MonoBehaviour
    {
        MeshFilter mf;
        Mesh mesh;
        public GameObject[] m_FingerTargets;
        public HashSet<GameObject> m_UpdateLastFrame = new HashSet<GameObject>();
        private HandsController m_HandController;
        public float tweenSpeed = 1.0f;
        public int m_TrackedHandId = 0;
        public bool m_updateContour = false;
        public int m_meshVerticies = 500;
        private Vector3[] meshDataVertex;
        private int[] meshDataTriangles;
        public bool m_updateRotationFromContour = true;
        public GameObject m_handCenterMarker;
        public GameObject m_handTipMarker;
        public float RotationSpeed = 1.0f;

        // Use this for initialization
        void Start()
        {
            m_HandController = FindObjectOfType<HandsController>();
            if (m_HandController != null)
            {
                m_HandController.m_HandDetectedEvent.AddListener(onEventHands);
            }
            mf = GetComponent<MeshFilter>();
            mesh = new Mesh();
            mf.sharedMesh = mesh;
            meshDataVertex = new Vector3[m_meshVerticies];
            mesh.vertices = meshDataVertex;
            meshDataTriangles = new int[m_meshVerticies * 3];
        }


        // Update is called once per frame
        void Update()
        {
            foreach (GameObject o in m_FingerTargets)
                if (!m_UpdateLastFrame.Contains(o))
                    o.transform.localPosition = Vector3.Lerp(o.transform.localPosition, m_handCenterMarker.transform.localPosition, Time.deltaTime * tweenSpeed);
            m_UpdateLastFrame.Clear();
        }

        void onEventHands(int handId, Hand hand)
        {
            if (handId.Equals(m_TrackedHandId))
            {
                if (m_handCenterMarker != null)
                    m_handCenterMarker.transform.localPosition = Vector3.Lerp(m_handCenterMarker.transform.localPosition, hand.center, Time.deltaTime * tweenSpeed);
                if (hand.fingers != null)
                {
                    Debug.Log("onEventHands Valid fingers: " + hand.finger_count + " out of: " + hand.fingers.Length);
                    for (int i = 0; i < hand.finger_count; i++)
                    {
                        Vector3 f = hand.fingers[i];
                        if (f.z > hand.center.z+0.02f)
                            setClosestFingerTarget(hand.fingers[i]);
                    }
                }
                if (m_updateContour)
                {
                    updateContour(handId);
                }
            }
        }

        private void setClosestFingerTarget(Vector3 finger)
        {
            GameObject closest = null;
            float minDist = float.MaxValue;
            foreach (GameObject target in m_FingerTargets)
            {
                if (m_UpdateLastFrame.Contains(target))
                    continue;
                float dist = Vector3.Distance(target.transform.localPosition, finger);
                if (dist < minDist)
                {
                    closest = target;
                    minDist = dist;
                };
            }
            if (closest != null)
            {
                closest.transform.localPosition = Vector3.Lerp(closest.transform.localPosition, finger, Time.deltaTime * tweenSpeed);
                m_UpdateLastFrame.Add(closest);
            }
        }

        private void updateContour(int handId)
        {
            int pointCount = Realsense.HandDetector.GetHandMesh(handId, meshDataVertex, meshDataVertex.Length);
            if (m_updateRotationFromContour)
            {
                if (m_handCenterMarker != null)
                    m_handCenterMarker.transform.localRotation = getRotation(m_handCenterMarker.transform.localPosition, meshDataVertex);
            }
            if (mf != null)
            {
                if (pointCount > 0)
                {
                    mesh.Clear();
                    mesh.vertices = meshDataVertex;
                    int[] indices = new int[pointCount];
                    for (int i = 0; i < indices.Length; i++)
                    {
                        indices[i] = i;
                    }
                    mesh.SetIndices(indices, MeshTopology.LineStrip, 0);
                }
            }
        }
        private Quaternion getRotation(Vector3 center, Vector3[] meshData)
        {
            Vector3 maxV = center;
            foreach (Vector3 v in meshData)
            {
                if (v.z > maxV.z)
                    maxV = v;
            }
            if (m_handTipMarker != null)
                m_handTipMarker.transform.localPosition = Vector3.Lerp(m_handTipMarker.transform.localPosition, maxV, Time.deltaTime * tweenSpeed);
            //find the vector pointing from our position to the target
            Vector3 _direction = (maxV - center).normalized;
            var _lookRotation = Quaternion.LookRotation(_direction);
            //create the rotation we need to be in to look at the target
            return Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);
        }

        public class EventHandDetected : UnityEvent<int, Hand> { };

    }
}