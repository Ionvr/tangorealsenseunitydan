﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class PointcloudController : MonoBehaviour
{
    const int MAX_MESH_VERTICIES = 65000;
    const int WORLD_DATA_SIZE = 320 * 240;
    Vector3[] worldData;
    MeshFilter mf;
    Mesh mesh;
    public float nearPlane = 0.05f;
    public float farPlane = 8.0f;
    public bool updateMesh = true;
    int pointCount = 0;
    // Use this for initialization
    void Start()
    {
        worldData = new Vector3[MAX_MESH_VERTICIES];
        mf = GetComponent<MeshFilter>();
        if (mf != null)
        {
            mesh = mf.mesh;
            mesh.Clear();
        }

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("RealsensePointCloud Got points count: " + pointCount);
        if (mf != null && mesh != null)
        {
            //Debug.Log("Realsense Pointcloud returned " + pointCount+" points");
            if (updateMesh)
            {
                pointCount = Realsense.Pointcloud.GetWorld(worldData, worldData.Length, nearPlane, farPlane);

                if (worldData != null && pointCount > 0)
                {
                    // Need to update indicies too!
                    int[] indices = new int[pointCount];
                    for (int i = 0; i < indices.Length; ++i)
                    {
                        indices[i] = i;
                    }
                    mesh.Clear();
                    mesh.vertices = worldData;
                    mesh.SetIndices(indices, MeshTopology.Points, 0);
                }
            }
        }
    }
}
