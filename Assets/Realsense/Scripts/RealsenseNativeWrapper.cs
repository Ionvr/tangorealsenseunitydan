﻿using UnityEngine;

namespace Realsense
{
    using System;
    using System.Runtime.InteropServices;
    public class Camera
    {
#if UNITY_ANDROID
        public const string MODULE_NAME = "nativeRealsenseImageProcessing";
#else
	public const string MODULE_NAME = "nativeRealsenseImageProcessing.dll";
#endif
    }
    public class Pointcloud
    {

        [DllImport(Camera.MODULE_NAME, EntryPoint = "RealsenseCVCaptureGetWorld")]
        public static extern int GetWorld([In, Out] Vector3[]
                                          pointArray, int size, float nearPlane, float farPlane);




    }
    public class HandDetector
    {
    
        [DllImport(Camera.MODULE_NAME, EntryPoint = "RealsenseCVHandDetectorSetParams")]
        public static extern void SetParam(HandParams handParams);
        [DllImport(Camera.MODULE_NAME, EntryPoint = "RealsenseCVHandDetectorFindHands")]
        public static extern int FindHands();
        [DllImport(Camera.MODULE_NAME, EntryPoint = "RealsenseCVHandDetectorGetHand")]
        public static extern bool GetHand(int handId,ref Hand hand);
        [DllImport(Camera.MODULE_NAME, EntryPoint = "RealsenseCVHandDetectorGetHandMesh")]
        public static extern int GetHandMesh(int handId, [In, Out] Vector3[] meshDataVertex,  int length);



    }
    [StructLayout(LayoutKind.Sequential)]
    public struct HandParams
    {
        public int handCount;
        public int minAreaSize;
        public int maxAreaSize;
        public int minDefectDepth;
        public int maxDefectDepth;
        public int minDepthTreshold;
        public int maxDepthTreshold;
        public string toString()
        {
            string str = "HandParams - ";
            str += "handCount: " + handCount;
            str += " minAreaSize: " + minAreaSize;
            str += " maxAreaSize: " + maxAreaSize;
            str += " minDefectDepth: " + minDefectDepth;
            str += " maxDefectDepth: " + maxDefectDepth;
            str += " minDepthTreshold: " + minDepthTreshold;
            str += " maxDepthTreshold: " + maxDepthTreshold;
            return str;
        }
    }
    public struct Hand
    {
        private static readonly int FINGER_COUNT = 10;
        public Vector3 center;
        public int contour_area;
        public int finger_count;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public Vector3[] fingers;
        

        public static Hand[] createArray(int count)
        {
            Hand[] arr = new Hand[count];
            for (int i = 0; i < count; i++)
            {
                arr[i].fingers = new Vector3[FINGER_COUNT];
                for (int j = 0; j < FINGER_COUNT; j++)
                    arr[i].fingers[j] = Vector3.zero;
            }
            return arr;
        }
    }
}